﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media;
using Palmmedia.WpfGraph.Common;
using Palmmedia.WpfGraph.Core;
using Palmmedia.WpfGraph.UI.Properties;
using Palmmedia.WpfGraph.UI.ViewModels;
using System.Windows.Controls;

namespace Palmmedia.WpfGraph.UI.Algorithms.SpanningTree
{
    /// <summary>
    ///  Dijkstra´s algoritmo.
    /// en.wikipedia.org/wiki/Dijkstra's_algorithm
    /// </summary>
    internal class Dijkstra : IGraphAlgorithm
    {
        /// <summary>
        /// The name of the algorithm.
        /// </summary>
        private const string NAME = "Dijkstra";

        /// <summary>
        /// Dictionary containing the distance to each node.
        /// </summary>
        private Dictionary<Node<NodeData, EdgeData>, double> node2DistanceDictionary;

        /// <summary>
        /// Nodos no visitados.
        /// </summary>
        private HashSet<Node<NodeData, EdgeData>> unvisitedNodes;

        /// <summary>
        /// Nombre del algoritmo
        /// </summary>
        public string Name
        {
            get
            {
                return NAME;
            }
        }

        /// <summary>
        /// Agrega la categoria al menu
        /// </summary>
        public string Category
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Ejecuta el algoritmo.
        /// </summary>
        /// <param name="graph">The graph.</param>
        /// <exception cref="System.InvalidOperationException">Thrown if graph does not meet special demands required by the graph algorithm.</exception>
        public void Execute(IGraph<NodeData, EdgeData> graph)
        {
            //Verifica que la distancia de los nodos sea positivo
            if (graph.Edges.Count(e => e.Data.Weight <= 0) > 0)
            {
                throw new InvalidOperationException(GraphAlgorithmErrors.EdgeWeightsNegative);
            }

            var markedNodes = graph.Nodes.Where(n => n.Data.Marked);

            // Verifica que tenga un Nodo marcado
            if (markedNodes.Count() != 1)
            {
                throw new InvalidOperationException(GraphAlgorithmErrors.OneNodeMarked);
            }

            this.unvisitedNodes = graph.Nodes.ToHashSet();

            // Inicia la distancia
            this.node2DistanceDictionary = graph.Nodes.ToDictionary(n => n, n => double.MaxValue);

            // Inicia el algoritmo en el nodo marcado
            var nextNode = markedNodes.First();
            this.node2DistanceDictionary[nextNode] = 0;

            nextNode.Blink(() => this.ProcessNode(nextNode));
        }

        /// <summary>
        /// Procede al siguiente nodo.
        /// </summary>
        /// <param name="currentNode">The node.</param>
        private void ProcessNode(Node<NodeData, EdgeData> currentNode)
        {
            currentNode.ChangeColor(Colors.SteelBlue);
            

            double currentWeight = this.node2DistanceDictionary[currentNode];

            foreach (var edge in currentNode.OutgoingEdges.Where(e => e.FirstNode != e.SecondNode))
	        {
                var targetNode = edge.FirstNode == currentNode ? edge.SecondNode : edge.FirstNode;

                double newWeight = currentWeight + edge.Data.Weight;

                //si la arista tiene menor la menor distancia al nodo, se actualiza la nueva distancia
                if (newWeight < this.node2DistanceDictionary[targetNode])
                {
                    edge.ChangeColor(Colors.Yellow);
                   
                    //currentNode.Data.Text
                 
                    this.node2DistanceDictionary[targetNode] = newWeight;
                        
                }
	        }

            this.unvisitedNodes.Remove(currentNode);

            // Continue with unvisited node with the minimum distance
            var nextNode = this.unvisitedNodes.OrderBy(n => this.node2DistanceDictionary[n]).FirstOrDefault();

            if (nextNode != null)
            {
                nextNode.Blink(() => this.ProcessNode(nextNode));
            }
        }
    }
}